# -*- coding: utf-8 -*-
{
    'name': 'Loan Order',
    'version': '1.0',
    'depends': ['stock', 'sale', 'account'],
    'author': 'Port Cities',
    'description': """ """,
    'website': 'http://www.portcities.net',
    'category': 'Warehouse',
    'sequence': 5,
    'summary': 'Create Loan Order',
    'data': [
        'data/ir_sequence_data.xml',
        'security/loan_order_security.xml',
        'security/ir.model.access.csv',
        'views/loan_order_views.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}

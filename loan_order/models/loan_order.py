# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class loan_order(models.Model):
    _name = 'loan.order'
    _description = 'Loan Order'

    name = fields.Char(string='Order Number',
                       required=True, copy=False,
                       readonly=True, index=True,
                       default=lambda self: _('New'))
    partner_id = fields.Many2one('res.partner', string='Customer')
    order_date = fields.Datetime(string='Loan Date',
                                 default=fields.Datetime.now)
    start_date = fields.Date(string='Start Date')
    end_date = fields.Date(string='End Date')
    state = fields.Selection([
        ('draft', 'Draft Loan'),
        ('confirm', 'Loan Sent'),
        ('partial', 'Loan Order'),
        ('delivered', 'Delivered'),
        ('returned', 'Returned')
    ], string='Status', default='draft')
    loan_order_ids = fields.One2many('loan.order.line', 'loan_order_id',
                                     string='Order Line')
    reference = fields.Char(string='Related SO')

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('loan.order') or _('New')

        return super(loan_order, self).create(vals)

    @api.multi
    def action_return(self):
        self.write({'state': 'returned'})

    @api.multi
    def action_confirm(self):
        self.write({'state': 'confirm'})

    @api.multi
    def action_partial(self):
        self.write({'state': 'partial'})

    @api.multi
    def action_deliver(self):
        self.write({'state': 'delivered'})

    @api.multi
    def action_draft(self):
        self.write({'state': 'draft'})


class loan_order_line(models.Model):
    _name = 'loan.order.line'
    _description = 'Loan Order Line'
    _order = 'sequence'

    product_id = fields.Many2one('product.product',
                                 string='Product', required=True)
    name = fields.Text(string='Description', required=True)
    qty = fields.Float(string='Quantity', required=True)
    stock = fields.Float(related='product_id.qty_available',
                         string='Free Stock')
    sequence = fields.Integer(string='Sequence', default=0)
    loan_order_id = fields.Many2one('loan.order', string='Order Reference',
                                    required=True, ondelete='cascade',
                                    index=True, copy=False)

    @api.multi
    @api.onchange('product_id', 'qty')
    def product_id_change(self):
        if not self.product_id:
            return None

        name = self.product_id.name_get()[0][1]
        if self.product_id.description_sale:
            name += '\n' + self.product_id.description_sale

        self.update({'name': name})

        if self.stock == 0:
            warning_mess = {
                'title': _('No Stock Available!'),
                'message':
                _('The product is not available on the stock.')
            }
            return {'warning': warning_mess}

        if (self.qty > self.stock):
            warning_mess = {
                'title': _('Not Enough Stock!'),
                'message':
                _('The amount you input exceed the amount in stock.')
            }
            return {'warning': warning_mess}

odoo.define('custom_pos.pos', function (require) {
    "use strict";

    var screen = require('point_of_sale.screens');
    var gui = require('point_of_sale.gui');
    var models = require('point_of_sale.models');
    var core = require('web.core');
    var Model = require('web.DataModel');
    var utils = require('web.utils');
    var formats = require('web.formats');

    var QWeb = core.qweb;
    var _t = core._t;

    var round_pr = utils.round_precision;

    console.log("Test");

    // var CustomOrderWidget = PosBaseWidget.extend({
    //     get_image_url: function(product){
    //         return window.location.origin + '/web/image?model=product.product&field=image_medium&id='+product.id;
    //     },
    // });

    var CustomProductScreenWidget = screen.ProductScreenWidget.include({
        template:'ProductScreenWidget',

        deleteorder_click_custom: function(event, $el) {
            var self  = this;
            var order = this.pos.get_order(); 
            if (!order) {
                return;
            } else if ( !order.is_empty() ){
                self.pos.delete_current_order();
            } else {
                this.pos.delete_current_order();
            }
        },
        renderElement: function(){
            var self = this;
            this._super();

            this.$('.deleteorder-button-custom').click(function(event){
                self.deleteorder_click_custom(event,$(this));
            });
        },
    });
});
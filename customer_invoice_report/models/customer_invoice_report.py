# coding: utf-8
from openerp import models, fields, api, _
from datetime import datetime
from cStringIO import StringIO
import xlsxwriter
import base64


class CustomerInvoiceReport(models.TransientModel):
    _name = "customer.invoice.report"
    _description = "Customer Invoice Report"

    all_customers = fields.Boolean('All Customers', default=True)
    partner_id    = fields.Many2one('res.partner', 'Customer')
    inv_date      = fields.Datetime('Date / Time', default=datetime.today())
    state_x       = fields.Selection([('choose', 'choose'), ('get', 'get')], default='choose')
    data_x        = fields.Binary('File', readonly=True)
    name          = fields.Char('Filename', size=100, readonly=True)

    @api.multi
    def excel_report(self):
        data = {}

        # if self.partner_id:
        #     list_invoice = self.env['account.invoice'].search(['|', ('state', '=', 'open'), ('state', '=', 'paid'), ('type', '=', 'out_invoice'), ('date_invoice', '>=', self.inv_date), ('partner_id', '=', self.partner_id.id)])
        # else:
        #     list_invoice = self.env['account.invoice'].search(['|', ('state', '=', 'open'), ('state', '=', 'paid'), ('type', '=', 'out_invoice'), ('date_invoice', '>=', self.inv_date)])

        # for invoice in list_invoice:

        #     data[invoice] = {
        #         'Invoice Number' : invoice.number,
        #         'Customer Name'  : invoice.partner_id.name,
        #         'Product'        : [],
        #         'Qty'            : [],
        #         'Price'          : [],
        #         'Amount'         : [],
        #         'Invoice Date'   : datetime.strptime(invoice.date_invoice, '%Y-%m-%d').strftime('%d-%m-%Y'),
        #     }

        #     for line in invoice.invoice_line_ids:
        #         data[invoice]['Product'].append(line.product_id.name)
        #         data[invoice]['Qty'].append(line.quantity)
        #         data[invoice]['Price'].append(line.price_unit)
        #         data[invoice]['Amount'].append(line.price_subtotal)

        return self._print_excel_report(data)

    def _print_excel_report(self, data):
        fp = StringIO()
        workbook = xlsxwriter.Workbook(fp)
        filename = 'Inventory_Report_' + datetime.today().strftime('%d-%m-%Y') + '.xlsx'

        #### STYLE ####
        #################################################################################
        top_style = workbook.add_format({'bold': 1, 'valign': 'vcenter'})
        top_style.set_font_name('Arial')
        top_style.set_font_size('15')
        top_style.set_text_wrap()
        #################################################################################
        top_style2 = workbook.add_format({'bold': 1, 'valign': 'vcenter'})
        top_style2.set_font_name('Arial')
        top_style2.set_font_size('12')
        top_style2.set_text_wrap()
        #################################################################################
        header_style = workbook.add_format({'bold': 1, 'align': 'center', 'valign': 'vcenter'})
        header_style.set_font_name('Arial')
        header_style.set_font_size('11')
        header_style.set_border()
        header_style.set_text_wrap()
        header_style.set_bg_color('#BDC3C7')
        #################################################################################
        normal_style = workbook.add_format({'valign': 'vcenter'})
        normal_style.set_border()
        normal_style.set_text_wrap()
        normal_style.set_font_name('Arial')
        normal_style.set_font_size('11')
        #################################################################################
        normal_center = workbook.add_format({'valign': 'vcenter', 'align': 'center'})
        normal_center.set_border()
        normal_center.set_text_wrap()
        normal_center.set_font_name('Arial')
        normal_center.set_font_size('11')
        #################################################################################
        normal_float = workbook.add_format({'valign': 'vcenter', 'align': 'center'})
        normal_float.set_border()
        normal_float.set_text_wrap()
        normal_float.set_num_format('#,##0.00;-#,##0.00')
        normal_float.set_font_name('Arial')
        normal_float.set_font_size('11')
        #################################################################################
        tot_style = workbook.add_format({'bold': 1, 'align': 'left', 'valign': 'vcenter'})
        tot_style.set_font_name('Arial')
        tot_style.set_font_size('12')
        tot_style.set_border()
        tot_style.set_text_wrap()
        tot_style.set_bg_color('#BDC3C7')
        #################################################################################
        tot_center = workbook.add_format({'bold': 1, 'align': 'center', 'valign': 'vcenter'})
        tot_center.set_font_name('Arial')
        tot_center.set_font_size('12')
        tot_center.set_border()
        tot_center.set_text_wrap()
        tot_center.set_bg_color('#BDC3C7')
        #################################################################################
        tot_float = workbook.add_format({'bold': 1, 'align': 'center', 'valign': 'vcenter'})
        tot_float.set_font_name('Arial')
        tot_float.set_font_size('12')
        tot_float.set_num_format('#,##0.00;-#,##0.00')
        tot_float.set_border()
        tot_float.set_text_wrap()
        tot_float.set_bg_color('#BDC3C7')
        #################################################################################
        merge_formats = workbook.add_format({'bold': 1, 'align': 'left', 'valign': 'vcenter'})
        merge_formats.set_font_name('Arial')
        merge_formats.set_font_size('12')
        #################################################################################
        worksheet = workbook.add_worksheet("Customer Invoice")
        worksheet.set_column('A:A', 20)
        worksheet.set_column('B:B', 20)
        worksheet.set_column('C:C', 30)
        worksheet.set_column('D:D', 20)
        worksheet.set_column('E:E', 20)
        worksheet.set_column('F:F', 20)
        worksheet.set_column('G:G', 20)
        #################################################################################

        worksheet.merge_range('A1:C1', 'Customer Invoice', merge_formats)
        worksheet.write('A2', 'Date', merge_formats)
        worksheet.write('B2', ': ' + datetime.today().strftime('%d-%m-%Y'), merge_formats)

        worksheet.write('A4', 'Invoice Number', header_style)
        worksheet.write('B4', 'Customer Name', header_style)
        worksheet.write('C4', 'Product', header_style)
        worksheet.write('D4', 'Qty', header_style)
        worksheet.write('E4', 'Price', header_style)
        worksheet.write('F4', 'Amount', header_style)
        worksheet.write('G4', 'Invoice Date', header_style)

        row = 4

        total_qty    = 0
        total_price  = 0
        total_amount = 0

        if self.partner_id:
            list_invoice = self.env['account.invoice'].search(['|', ('state', '=', 'open'), ('state', '=', 'paid'), ('type', '=', 'out_invoice'), ('date_invoice', '>=', self.inv_date), ('partner_id', '=', self.partner_id.id)])
        else:
            list_invoice = self.env['account.invoice'].search(['|', ('state', '=', 'open'), ('state', '=', 'paid'), ('type', '=', 'out_invoice'), ('date_invoice', '>=', self.inv_date)])

        for invoice in list_invoice:

            worksheet.write(row, 0, invoice.number, normal_style)
            worksheet.write(row, 1, invoice.partner_id.name, normal_style)
            worksheet.write(row, 6, datetime.strptime(invoice.date_invoice, '%Y-%m-%d').strftime('%d-%m-%Y'), normal_center)

            sum_qty    = 0
            sum_price  = 0
            sum_amount = 0

            for line in invoice.invoice_line_ids:
                worksheet.write(row, 2, line.product_id.name, normal_style)
                worksheet.write(row, 3, line.quantity, normal_center)
                worksheet.write(row, 4, line.price_unit, normal_float)
                worksheet.write(row, 5, line.price_subtotal, normal_float)

                worksheet.write(row + 1, 0, None, normal_style)
                worksheet.write(row + 1, 1, None, normal_style)
                worksheet.write(row + 1, 6, None, normal_style)

                sum_qty    += line.quantity
                sum_price  += line.price_unit
                sum_amount += line.price_subtotal

                row += 1

            worksheet.merge_range(row, 0, row, 2, 'Total', tot_style)
            worksheet.write(row, 3, sum_qty, tot_center)
            worksheet.write(row, 4, sum_price, tot_float)
            worksheet.write(row, 5, sum_amount, tot_float)
            worksheet.write(row, 6, None, tot_style)

            total_qty    += sum_qty
            total_price  += sum_price
            total_amount += sum_amount

            row += 1

        # for invoice in sorted(data, key=lambda invoice: (data[invoice]['Invoice Date'], invoice.id), reverse=True):
        #     worksheet.write(row, 0, data[invoice]['Invoice Number'], normal_style)
        #     worksheet.write(row, 1, data[invoice]['Customer Name'], normal_style)
        #     worksheet.write(row, 6, data[invoice]['Invoice Date'], normal_center)

        #     line_product = data[invoice]['Product']
        #     line_qty     = data[invoice]['Qty']
        #     line_price   = data[invoice]['Price']
        #     line_amount  = data[invoice]['Amount']

        #     sum_qty    = 0
        #     sum_price  = 0
        #     sum_amount = 0

        #     for a,b,c,d in zip(line_product, line_qty, line_price, line_amount):
        #         worksheet.write(row, 2, a, normal_style)
        #         worksheet.write(row, 3, b, normal_center)
        #         worksheet.write(row, 4, c, normal_float)
        #         worksheet.write(row, 5, d, normal_float)

        #         worksheet.write(row + 1, 0, None, normal_style)
        #         worksheet.write(row + 1, 1, None, normal_style)
        #         worksheet.write(row + 1, 6, None, normal_style)

        #         sum_qty    += b
        #         sum_price  += c
        #         sum_amount += d

        #         row += 1

        #     worksheet.merge_range(row, 0, row, 2, 'Total', tot_style)
        #     worksheet.write(row, 3, sum_qty, tot_center)
        #     worksheet.write(row, 4, sum_price, tot_float)
        #     worksheet.write(row, 5, sum_amount, tot_float)
        #     worksheet.write(row, 6, None, tot_style)

        #     total_qty    += sum_qty
        #     total_price  += sum_price
        #     total_amount += sum_amount

        #     row += 1

        worksheet.merge_range(row, 0, row, 2, 'Grand Total', tot_style)
        worksheet.write(row, 3, total_qty, tot_center)
        worksheet.write(row, 4, total_price, tot_float)
        worksheet.write(row, 5, total_amount, tot_float)
        worksheet.write(row, 6, None, tot_style)

        workbook.close()
        out = base64.encodestring(fp.getvalue())
        self.write({'state_x': 'get', 'data_x': out, 'name': filename})
        ir_model_data = self.env['ir.model.data']
        fp.close()

        form_res = ir_model_data.get_object_reference('customer_invoice_report', 'customer_invoice_report_wizard_view')
        form_id  = form_res and form_res[1] or False

        return {
            'name'      : _('Download XLS'),
            'view_type' : 'form',
            'view_mode' : 'form',
            'res_model' : 'customer.invoice.report',
            'res_id'    : self.id,
            'view_id'   : False,
            'views'     : [(form_id, 'form')],
            'type'      : 'ir.actions.act_window',
            'target'    : 'current',
        }

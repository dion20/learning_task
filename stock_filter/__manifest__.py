# -*- coding: utf-8 -*-
{
    'name': 'Stock Filter',
    'version': '1.0',
    'depends': ['stock'],
    'author': 'Port Cities',
    'website': 'http://www.portcities.net',
    'data': ['views/stock_filter_views.xml'],
    'auto_install': False,
    'installable': True,
    'application': True,
}

# coding: utf-8

from openerp import models, fields, api, exceptions, _
from datetime import datetime


# Task 1 #

class Task_1_to_4(models.Model):
    _inherit = 'purchase.order'

    supplier_phone = fields.Char(related='partner_id.phone', string='Phone')
    supplier_email = fields.Char(related='partner_id.email', string='Email')

# Task 2 #

    contact_person = fields.Many2one('res.users', string='Contact Person',
                                     default=lambda self: self.env.user)

# Task 3 #

    @api.one
    @api.depends('minimum_planned_date', 'date_order')
    def _compute_duration(self):
        if not self.minimum_planned_date:
            self.minimum_planned_date = fields.Date.today()
        dp = fields.Date.from_string(self.minimum_planned_date)
        do = fields.Date.from_string(self.date_order)
        if dp > do:
            self.duration = (dp - do).days
        else:
            self.duration = 0

    duration = fields.Float('Duration', compute='_compute_duration')

# Task 4 #

    @api.multi
    def action_view_related_products(self):
        term_domain = []
        for line in self.order_line:
            term_domain.append(line.id)
        domain = [('id', 'in', term_domain)]
        return {
            'name': ('Related Products'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree, form',
            'res_model': 'product.template',
            'target': 'current',
            'domain': domain
        }


# Task 5 #

class Task_5(models.Model):
    _inherit = 'stock.picking'

    confirm_date = fields.Datetime(string='Confirmed Date')
    confirm_user = fields.Many2one('res.users', string='Confirmed By')

    @api.multi
    @api.depends('confirm_date', 'confirm_user')
    def action_confirm(self):
        self.confirm_date = fields.Date.today()
        self.confirm_user = self.env.user
        return super(Task_5, self).action_confirm()


# Task 6 #

class Task_6(models.Model):
    _inherit = 'purchase.order'

    @api.model
    def create(self, values):
        if not self.partner_ref:
            values['partner_ref'] = 'No Supplier Reference'
        return super(Task_6, self).create(values)

    @api.multi
    def write(self, values):
        if not self.partner_ref:
            values['partner_ref'] = 'No Supplier Reference'
        return super(Task_6, self).write(values)


# Task 7 #

class Task_7(models.Model):
    _inherit = 'product.template'

    @api.model
    def create(self, values):
        if not self.seller_ids:
            res_partner_obj = self.env['res.partner'].search([
                ('name', '=', 'No Supplier')])
            prod_supplier_obj = self.env['product.supplierinfo']
            prod_supplier_obj.create({
                'name': res_partner_obj.id,
                'product_tmpl_id': self.product_tmpl_id
            })
        return super(Task_7, self).create(values)

    @api.multi
    def write(self, values):
        if not self.seller_ids:
            values['seller_ids'] = [[0, False, {'name': 75}]]
        return super(Task_7, self).write(values)


# Task 8 #

class Task_8a(models.Model):
    _inherit = 'stock.picking.type'

    using_nik = fields.Boolean(string='Using NIK in Stock Picking')


class Task_8b(models.Model):
    _inherit = 'stock.picking'

    nik_number = fields.Char(string='NIK Number')


class Task_8c(models.TransientModel):
    _inherit = 'stock.transfer_details'

    using_nik = fields.Boolean(string='Using NIK in Stock Picking')
    nik_number = fields.Char(string='NIK Number')

    @api.model
    def default_get(self, values):
        res = super(Task_8c, self).default_get(values)
        stockpick = self.env['stock.picking'].search([
            ('id', '=', self._context.get('active_id'))])
        nik_usage = self.env['stock.picking.type'].search([
            ('id', '=', stockpick.picking_type_id.id)])
        res['using_nik'] = nik_usage.using_nik
        return res

    @api.multi
    def do_detailed_transfer(self):
        number = self.env['stock.picking'].search([
            ('id', '=', self.picking_id.id)])
        number.write({'nik_number': self.nik_number})
        return super(Task_8c, self).do_detailed_transfer()


# Task 9 #

class Task_9(models.Model):
    _inherit = 'purchase.order'

    down_payment = fields.Float(string='Down Payment (DP)', store=True)

    @api.one
    @api.depends('amount_total', 'down_payment')
    def get_amount_total_dp(self):
        if not self.down_payment:
            self.down_payment = 0
        tp = self.amount_total
        dp = self.down_payment
        self.amount_total_dp = (tp - dp)

    amount_total_dp = fields.Float(string='Total', store=True,
                                   compute='get_amount_total_dp')


# Task 10 #

# class Task_10(models.Model):
#     _inherit = 'product.template'

#     @api.model
#     def create(self, values):
#         if not self.seller_ids:
#             raise exceptions.Warning(
#                 'You should fill in the supplier details, at least one.')
#         return super(Task_10, self).create(values)

#     @api.multi
#     def write(self, values):
#         if not self.seller_ids:
#             raise exceptions.Warning(
#                 'You should fill in the supplier details, at least one.')
#         return super(Task_10, self).write(values)


# Task Draft #

class task_draft(models.Model):
    _inherit = 'product.template'

    @api.model
    def product_image(self, product_id):
        product_image = self.search([('id', '=', product_id)])[0].image_medium
        return product_image
